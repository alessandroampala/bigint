#include "BigInt.h"

BigInt::BigInt() : sign(true)
{
	//Initialize to zero in default constructor
	number.push_back(0);
}

BigInt::BigInt(int64_t n) : sign(n >= 0)
{
	//Convert the number n to the base "base"
	if (n < 0) n *= -1;
	while (n >= base)
	{
		uint32_t reminder = static_cast<uint32_t>(n % base);
		number.push_back(reminder);
		n /= base;
	}
	number.push_back(static_cast<uint32_t>(n));
}

BigInt::BigInt(std::string s)
{
	uint64_t b = 1;

	//Insert each digit of the string with BigInt sum
	for (int64_t i = s.size() - 1; i >= 0; --i)
	{
		BigInt digit10 = s[i] - '0';
		*this += digit10 * b;
		b *= 10;
	}
}

BigInt BigInt::operator+(const BigInt& rhs) const
{
	BigInt result;
	if (this->sign == rhs.sign)
	{
		result = UnsignedSum(*this, rhs);
		result.sign = this->sign;
	}
	else
	{
		if (this->sign) //rhs.sign == false
		{
			if (this->UnsignedGreaterThan(rhs)) //*this + rhs > 0
			{
				result = UnsignedSubtraction(*this, rhs);
				result.sign = true; //because this is the biggest number
			}
			else //*this + rhs < 0
			{
				result = UnsignedSubtraction(rhs, *this);
				result.sign = false;
			}
		}
		else // rhs > 0
		{
			if (rhs.UnsignedGreaterThan(*this)) //rhs + this > 0
			{
				result = UnsignedSubtraction(rhs, *this);
				result.sign = true;
			}
			else //*this + rhs < 0
			{
				result = UnsignedSubtraction(*this, rhs);
				result.sign = false;
			}
		}
	}

	result.FixZero();
	return result;
}

BigInt BigInt::operator+() const
{
	return *this;
}

BigInt BigInt::operator-(const BigInt& rhs) const
{
	BigInt result;
	if (this->sign == rhs.sign)
	{
		result = UnsignedSubtraction(*this, rhs);

		if (this->sign)
			result.sign = this->UnsignedGreaterThan(rhs) || this->number == rhs.number;
		else
			result.sign = this->UnsignedLessThan(rhs);
	}
	else
	{
		if (this->sign)
		{
			result = UnsignedSum(*this, rhs);
			result.sign = true;
		}
		else
		{
			if (rhs.UnsignedGreaterThan(*this))
			{
				result = UnsignedSubtraction(rhs, *this);
				result.sign = true;
			}
			else
			{
				result = UnsignedSubtraction(*this, rhs);
				result.sign = false;
			}
		}
	}

	result.FixZero();
	return result;
}

BigInt& BigInt::operator-=(BigInt const& rhs)
{
	*this = *this - rhs;
	return *this;
}

BigInt BigInt::operator-() const
{
	BigInt r = *this;
	r.sign = !r.sign; //Flip sign
	return r;
}

BigInt BigInt::operator*(const BigInt& rhs) const
{
	BigInt r = UnsingedMultiplication(*this, rhs);
	r.sign = this->sign == rhs.sign;
	return r;
}

BigInt BigInt::operator/(const BigInt& rhs) const
{
	BigInt rhs2 = rhs;
	rhs2.sign = true;
	BigInt r = UnsignedDivision(*this, rhs2);
	r.sign = this->sign == rhs.sign;
	return r;
}

uint32_t BigInt::operator%(const BigInt& rhs) const
{
	//Take the reminder
	return (*this - (*this / rhs * rhs)).number.front();
}

BigInt& BigInt::operator*=(BigInt const& rhs)
{
	*this = *this * rhs;
	return *this;
}

BigInt& BigInt::operator+=(BigInt const& rhs)
{
	*this = *this + rhs;
	return *this;
}

BigInt& BigInt::operator%=(BigInt const& rhs)
{
	*this = *this % rhs;
	return *this;
}

bool BigInt::operator!=(const BigInt& rhs) const
{
	return !operator==(rhs);
}

bool BigInt::operator==(const BigInt& rhs) const
{
	//negative/positive 0 sign case
	if (this->IsZero() && rhs.IsZero())
		return true;

	return this->sign == rhs.sign && this->number == rhs.number;
}

BigInt BigInt::operator++(int)
{
	BigInt r = *this;
	++(*this);
	return r;
}

BigInt& BigInt::operator++()
{
	*this += 1;
	return *this;
}

BigInt BigInt::operator--(int)
{
	BigInt r = *this;
	--(*this);
	return r;
}

BigInt& BigInt::operator--()
{
	*this -= 1;
	return *this;
}

bool BigInt::operator>(const BigInt& rhs) const
{
	if (this->sign != rhs.sign) return this->sign;

	if (this->sign)
		return UnsignedGreaterThan(rhs);
	else
		return !UnsignedGreaterThan(rhs);
}

bool BigInt::operator>=(const BigInt& rhs) const
{
	return operator>(rhs) || operator==(rhs);
}

//Multiply a single digit and a BigInt
BigInt BigInt::MultDigit(const BigInt& a, uint32_t digit)
{
	BigInt r;

	for (int i = 0; i < a.number.size(); i++)
	{

		BigInt mid = (uint64_t)a.number[i] * digit;
		if (r.number.back() == 0 && i > 0 && mid > 0 && mid.number.size() == 1)
			r.number.push_back(mid.number.front());
		else
			r += mid;
	}
	return r;
}

bool BigInt::UnsignedEqualsTo(const BigInt& rhs) const
{
	return this->number == rhs.number;
}

bool BigInt::UnsignedGreaterThan(const BigInt& rhs) const
{
	if (this->number.size() > rhs.number.size()) return true;
	if (this->number.size() < rhs.number.size()) return false;

	//so this->number.size() == rhs.number.size()

	int64_t i = this->number.size() -1;
	while (i >= 0)
	{
		if (this->number[i] == rhs.number[i])
		{
			--i;
			continue;
		}
		if (this->number[i] > rhs.number[i])
			return true;
		else
			return false;
	}

	return false; //the numbers are equals
}

bool BigInt::operator<(const BigInt& rhs) const
{
	if (this->sign != rhs.sign) return !this->sign;

	if (this->sign)
		return UnsignedLessThan(rhs);
	else
		return !UnsignedLessThan(rhs);
}

bool BigInt::operator<=(const BigInt& rhs) const
{
	return operator<(rhs) || operator==(rhs);
}

BigInt& BigInt::operator<<=(size_t pos)
{
	*this = *this << pos;
	return *this;
}

BigInt BigInt::operator<<(size_t pos) const
{
	BigInt r = *this;

	//single shift left pos times
	for (size_t i = 0; i < pos; ++i)
	{
		// single shift left
		auto it = r.number.begin();
		uint32_t mask = (uint32_t)1 << 31;
		uint8_t previousHigherBit = (*it & mask) >> 31;
		*it <<= 1;
		it++;
		for (; it != r.number.end(); it++)
		{
			uint8_t higherBit = (*it & mask) >> 31;
			*it <<= 1;
			*it |= previousHigherBit;
			previousHigherBit = higherBit;
		}

		if (previousHigherBit == 1)
			r.number.push_back(1);
	}

	return r;
}

BigInt& BigInt::operator>>=(size_t pos)
{
	*this = *this >> pos;
	return *this;
}

BigInt BigInt::operator>>(size_t pos) const
{
	BigInt r = *this;

	//single shift right pos times
	for (size_t i = 0; i < pos; ++i)
	{
		// single shift right
		auto it = r.number.rbegin();
		uint8_t mask = 1;
		uint8_t previousLowerBit = *it & mask;
		*it >>= 1;
		++it;
		for (; it != r.number.rend(); ++it)
		{
			uint8_t lowerBit = *it & mask;
			*it >>= 1;
			*it |= previousLowerBit << 31;
			previousLowerBit = lowerBit;
		}

		r.FixZero();
	}

	return r;
}

BigInt BigInt::operator|(BigInt const& rhs) const
{
	BigInt r;
	r.number.clear();

	for (size_t i = 0; i < std::max(this->number.size(), rhs.number.size()); ++i)
	{
		uint32_t a = i < this->number.size() ? this->number[i] : 0;
		uint32_t b = i < rhs.number.size() ? rhs.number[i] : 0;
		
		r.number.push_back(a | b);
	}

	return r;
}

BigInt& BigInt::operator|=(BigInt const& rhs)
{
	*this = *this | rhs;
	return *this;
}

BigInt BigInt::operator&(BigInt const& rhs) const
{
	BigInt r;
	r.number.clear();

	for (size_t i = 0; i < std::max(this->number.size(), rhs.number.size()); ++i)
	{
		uint32_t a = i < this->number.size() ? this->number[i] : 0;
		uint32_t b = i < rhs.number.size() ? rhs.number[i] : 0;

		r.number.push_back(a & b);
	}

	r.FixZero();
	return r;
}

BigInt& BigInt::operator&=(BigInt const& rhs)
{
	*this = *this & rhs;
	return *this;
}

BigInt BigInt::operator^(BigInt const& rhs) const
{
	BigInt r;
	r.number.clear();

	for (size_t i = 0; i < std::max(this->number.size(), rhs.number.size()); ++i)
	{
		uint32_t a = i < this->number.size() ? this->number[i] : 0;
		uint32_t b = i < rhs.number.size() ? rhs.number[i] : 0;

		r.number.push_back(a ^ b);
	}

	return r;
}

BigInt& BigInt::operator^=(BigInt const& rhs)
{
	*this = *this ^ rhs;
	return *this;
}

BigInt BigInt::operator~() const
{
	BigInt r = *this;

	for (size_t i = 0; i < r.number.size(); ++i)
	{
		r.number[i] = ~r.number[i];
	}

	return r;
}

bool BigInt::UnsignedLessThan(const BigInt& rhs) const
{
	if (this->number.size() < rhs.number.size()) return true;
	if (this->number.size() > rhs.number.size()) return false;

	//so this->number.size() == rhs.number.size()
	return this->number.back() < rhs.number.back();
}

BigInt BigInt::UnsignedSum(const BigInt& a, const BigInt& b)
{
	BigInt r;
	uint32_t c = 0; //carry

	r.number.clear();

	size_t i;
	for (i = 0; i < std::max(a.number.size(), b.number.size()); ++i)
	{
		uint32_t aDigit = i < a.number.size() ? a.number[i] : 0;
		uint32_t bDigit = i < b.number.size() ? b.number[i] : 0;

		uint64_t s = (uint64_t) aDigit + bDigit + c;
		r.number.push_back(static_cast<uint32_t>(s)); //pushing the lower half part of s
		c = static_cast<uint32_t>(*((char*)&s + sizeof(uint32_t))); //taking the high half part of s
	}

	if (c != 0)
		r.number.push_back(c);

	return r;
}

BigInt BigInt::UnsignedSubtraction(const BigInt& a, const BigInt& b)
{
	const BigInt& aUNum = b.UnsignedGreaterThan(a) ? b : a;
	const BigInt& bUNum = b.UnsignedGreaterThan(a) ? a : b;

	BigInt r;
	char c = 0;

	r.number.clear();

	size_t i;
	for (i = 0; i < aUNum.number.size(); ++i)
	{
		uint32_t aDigit = i < aUNum.number.size() ? aUNum.number[i] : 0;
		uint32_t bDigit = i < bUNum.number.size() ? bUNum.number[i] : 0;

		uint64_t s = bDigit + c;
		if (s <= aDigit)
		{
			r.number.push_back(static_cast<uint32_t>(aDigit - s));
			c = 0;
		}
		else
		{
			r.number.push_back(static_cast<uint32_t>(aDigit + base - s));
			c = 1;
		}
	}

	return r;
}

BigInt BigInt::UnsingedMultiplication(const BigInt& aUNum, const BigInt& bUNum)
{
	const BigInt& a = bUNum.UnsignedGreaterThan(aUNum) ? bUNum : aUNum;
	const BigInt& b = bUNum.UnsignedGreaterThan(aUNum) ? aUNum : bUNum;

	BigInt r = 0;

	for (size_t i = 0; i < a.number.size(); ++i)
	{
		BigInt mid = MultDigit(b, a.number[i]);
		for (size_t j = 0; j < i; ++j)
			mid.number.push_front(0);
		r += mid;
	}

	return r;
}

BigInt BigInt::UnsignedDivision(const BigInt& aUNum, const BigInt& bUNum)
{
	/*
	* This algorithm follows the steps for BigInt division of
	* "Implementation of Unlimited Integer" paper.
	* The steps and substeps are marked respectively as "StepNum." and "(StepNum)"
	*/

	const BigInt& A = aUNum;
	const BigInt& B = bUNum;

	BigInt C;
	BigInt D;
	C.number.clear();
	D.number.clear();

	assert(B != 0); //cannot divide by 0

	uint64_t index = 0;

	//1.
	int64_t minBound = std::max((int64_t)((A.number.size() - index * B.number.size()) - B.number.size()), (int64_t)0);
	int64_t maxBound = (A.number.size() - index * B.number.size());

	while (maxBound >= 0)
	{
		//1.
		for (int64_t i = maxBound - 1; i >= minBound; --i)
		{
			D.number.push_front(A.number[i]);
		}

		//2.
		if (D.UnsignedLessThan(B))
		{
			C.number.push_front(0);
		}
		else if (D.UnsignedEqualsTo(B))
		{
			C.number.push_front(1);
			D = D - B;
		}
		else if (D.UnsignedGreaterThan(B))
		{
			//(1)
			if (D.number.size() == 1)
				D.number.push_back(0);
			uint64_t d2 = ((uint64_t)D.number[D.number.size() - 1] << 32) | (uint64_t)D.number[D.number.size() - 2];
			D.FixZero();
			uint64_t b = B.number[B.number.size() - 1];

			//finding q by dichotomy
			uint64_t min = std::min(d2 / (b + 1), b + 1);
			uint64_t max = std::min(d2 / b, base - 1);

			uint64_t q = (min + max) / 2;

			//(2)
			//find one suitable q settle for D <= B * (q + 1) and D >= B * q

			//variable for detecting infinite loop
			bool lock = false;
			bool maybeLock = false;
			while (true)
			{
				if (min >= max) break;

				if (min + 1 == max)
				{
					if (maybeLock)
						lock = true;
					maybeLock = true;
				}

				if (D > B * (q + 1))
				{
					min = q;
					q = (min + max) / 2;

					if (lock) q++;

					continue;
				}
				if (D < B * q)
				{
					max = q;
					q = (min + max) / 2;

					if (lock) q--;

					continue;
				}
				break;
			}

			if (D >= B * (q + 1))
				q = q + 1;

			if (q >= base)
				C.number.push_front(q >> 32);
			C.number.push_front(static_cast<uint32_t>(q));

			//(3)
			D.number.push_back(0);
			D -= B * q;
			D.number.pop_back();

		}

		//1.
		++index;
		minBound = std::max((int64_t)((A.number.size() - index * B.number.size()) - B.number.size()), (int64_t)0);
		maxBound = (A.number.size() - index * B.number.size());
	}

	C.FixZero();

	if (C * B + D != A)
		C.number.pop_front();
	return C;
}

//Returns true if the number represented is 0
bool BigInt::IsZero() const
{
	return number.size() == 1 && number.front() == 0;
}

//Returns true if all digits in number deque are 0
bool BigInt::AllZeros() const
{
	for (auto it = number.begin(); it != number.end(); it++)
	{
		if (*it != 0) return false;
	}
	return true;
}

//Remove heading zeros
void BigInt::FixZero()
{
	while (number.back() == 0 && number.size() > 1)
	{
		number.pop_back();
	}

	if (number.size() == 1 && number.front() == 0)
	{
		//The representation of 0 is always with positive sign
		if (!sign)
			sign = true;
	}
}
