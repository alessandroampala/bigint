#pragma once
#include <iostream>
#include <deque>
#include <cassert>

class BigInt
{
public:

	BigInt();
	BigInt(int64_t n);
	BigInt(std::string s);

	BigInt operator+(const BigInt& rhs) const;
	BigInt operator-(const BigInt& rhs) const;
	BigInt operator*(const BigInt& rhs) const;
	BigInt operator/(const BigInt& rhs) const;
	uint32_t operator%(const BigInt& rhs) const;

	BigInt& operator+=(BigInt const& rhs);
	BigInt& operator-=(BigInt const& rhs);
	BigInt& operator*=(BigInt const& rhs);
	BigInt& operator%=(BigInt const& rhs);

	BigInt& operator++();
	BigInt operator++(int);
	BigInt& operator--();
	BigInt operator--(int);

	BigInt operator+() const;
	BigInt operator-() const;

	bool operator==(const BigInt& rhs) const;
	bool operator!=(const BigInt& rhs) const;
	bool operator>(const BigInt& rhs) const;
	bool operator>=(const BigInt& rhs) const;
	bool operator<(const BigInt& rhs) const;
	bool operator<=(const BigInt& rhs) const;

	BigInt operator<<(size_t pos) const;
	BigInt& operator<<=(size_t pos);
	BigInt operator>>(size_t pos) const;
	BigInt& operator>>=(size_t pos);
	BigInt operator|(BigInt const& rhs) const;
	BigInt& operator|=(BigInt const& rhs);
	BigInt operator&(BigInt const& rhs) const;
	BigInt& operator&=(BigInt const& rhs);
	BigInt operator^(BigInt const& rhs) const;
	BigInt& operator^=(BigInt const& rhs);
	BigInt operator~() const;

	friend std::ostream& operator<< (std::ostream& out, BigInt const& n)
	{
		if (!n.sign)
			out << '-';

		for (auto it = n.number.rbegin(); it != n.number.rend(); ++it)
		{
			out << *it << ' ';
		}
		out << "(base 2^32)";
		return out;
	}

private:

	const static uint64_t base = 4294967296; //2^32
	std::deque<uint32_t> number;
	bool sign;

	static BigInt UnsignedSum(const BigInt& a, const BigInt& b);
	static BigInt UnsignedSubtraction(const BigInt& a, const BigInt& b);
	static BigInt UnsingedMultiplication(const BigInt& aUNum, const BigInt& bUNum);
	static BigInt UnsignedDivision(const BigInt& aUNum, const BigInt& bUNum);

	static BigInt MultDigit(const BigInt& a, uint32_t digit);

	bool UnsignedEqualsTo(const BigInt& rhs) const;
	bool UnsignedGreaterThan(const BigInt& rhs) const;
	bool UnsignedLessThan(const BigInt& rhs) const;

	bool IsZero() const;
	bool AllZeros() const;
	void FixZero();
};


