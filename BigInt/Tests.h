#pragma once

#include "BigInt.h"

class Test
{
public:
	void runTests()
	{
		testSumAndSubtraction();
		testMult();
		testDiv();
		testShifts();
		testBitwise();
		testIncAndDec();
		testStringConstructor();
	}

private:

	void testSumAndSubtraction()
	{
		BigInt p1 = 981981981981787;
		BigInt p2 = 78718;

		BigInt n1 = -981981981981787;
		BigInt n2 = -78718;

		assert(p1 + p2 == 981981982060505);
		assert(p1 - p2 == 981981981903069);
		assert(p1 - 981981981981786 == 1);
		assert(p2 - p1 == -981981981903069);
		assert(p2 + p1 == 981981982060505);

		assert(n1 + n2 == -981981982060505);
		assert(n1 - n2 == -981981981903069);
		assert(n2 + n1 == -981981982060505);
		assert(n2 - n1 == 981981981903069);

		assert(p1 + n1 == 0);
		assert(n1 + p1 == 0);
		assert(p2 + n2 == 0);
		assert(n2 + p2 == 0);

		assert(p1 + n2 == 981981981903069);
		assert(p1 - n2 == 981981982060505);
		assert(p2 + n1 == -981981981903069);
		assert(p2 - n1 == 981981982060505);
	}

	void testMult()
	{
		BigInt p1 = 981981981981787;
		BigInt p2 = 78718;

		assert(p2 * p2 * p2 == 487777938762232);
		assert(p2 * p2 * p2 * 0 == 0);
		assert(p2 * p2 * p2 * -1 == -487777938762232);
	}

	void testDiv()
	{
		BigInt a = 4;
		BigInt div = 4294967296;

		assert(a / 2 == 2);
		assert(a / 1 == 4);
		assert(a / 4 == 1);
		assert(a / 3 == 1);

		assert(div / 2 == (uint64_t)4294967296 / 2);
		assert(div / 4 == (uint64_t)4294967296 / 4);
		assert(div / div == 1);
		assert(div / 16 == (uint64_t)4294967296 / 16);
		assert((div * 2) / div == 2);
		assert((div * 4) / div == 4);
		assert(div / 2147483648 == 2);
		assert((div * 4294967296) / div == (uint64_t)4294967296);
		assert((div * 4294967296) / div / div == div / div);

		assert(div / -2 == (int64_t)4294967296 / -2);
		assert(div / -4 == (int64_t)4294967296 / -4);
	}

	void testShifts()
	{
		BigInt p1 = 981981981981787;
		BigInt p2 = 78718;

		assert(p2 << 1 == 78718 * 2);
		assert(p2 << 2 == p2 * 4);
		assert(p2 << 3 == p2 * 8);
		assert(p2 << 4 == p2 * 16);
		BigInt p3 = 1;

		assert(p2 >> 1 == 78718 / 2);
		assert(p2 >> 2 == 78718 / 4);
		assert(p2 >> 3 == 78718 / 8);
		assert(p2 >> 4 == 78718 / 16);
	}

	void testBitwise()
	{
		BigInt a = 4294967296;
		BigInt b = 2;

		assert((a | b) == 4294967296 + 2);
		assert((a & b) == 0);
		a |= b;
		assert(a == 4294967298);
		a = 4294967296;
		a &= b;
		assert(a == 0);
		a = 4294967295 - 1;
		b = 4294967295 - 2;
		assert((a ^ b) == 3);

		a = 4294967295 - 1;
		a ^= b;
		assert(a == 3);

		b = 4294967295;
		b += 4294967294;

		a = 4294967296;
		a = ~a;

		a = 4294967296;
		b = 4294967290;
		assert(a % b == 6);

		a = 4294967296;
		b = 4294967290;
		a %= b;
		assert(a == 6);
	}

	void testIncAndDec()
	{
		BigInt a = 1;
		a++;
		assert(a++ == 2);
		assert(++a == 4);
		assert(a-- == 4);
		assert(--a == 2);

		assert(+a == 2);
		assert(-a == -2);
	}

	void testStringConstructor()
	{
		BigInt a = 4294967296;
		a++;
		BigInt s("4294967297");
		assert(a == s);
	}
};