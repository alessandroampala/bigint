#include <iostream>
#include <assert.h>
#include "BigInt.h"
#include "Tests.h"

int main()
{
	Test t;
	t.runTests();
    std::cout << "All tests passed!\n";
}
