# BigInt
A C++ library for managing integers of arbitrary length.

## Solution adopted
The number is stored in a `deque` of `uint32_t` treating each element as a different digit, so each digit can contain a value that spans from 0 to 2^32 - 1. This is because the longest built in integer type is `uint64_t`, so we can use the latter to implement the basic **arithmetic operation** that would overflow the `uint32_t`.

This library handles sum, subtraction, multiplication and division in a primary school-algorithmic fashion, support signed numbers and **overloads all C++ operators** as well.

This implementation mostly follows the [Implementation of Unlimited Integer](https://ieeexplore.ieee.org/document/5283568) paper.
